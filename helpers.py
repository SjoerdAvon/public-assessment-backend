import os
import logging
from typing import Dict
from pathlib import Path
import yaml as yaml

logger = logging.getLogger(__name__)


def read_configuration_file() -> Dict:
    """
    Read the app configuration file to a dict
    """
    # Determine the current path and the config file locations in an os neutral manner
    logger.warning(">>> Determining config file paths")
    file_path = Path(__file__).parent.absolute()
    file_path_config = str(file_path) + os.path.sep + "config.yaml"

    # Setup the configuration dictionary with the values in the config file
    logger.warning(">>> Loading config file")
    with open(file_path_config) as file:
        config_dict = yaml.load(file, Loader=yaml.FullLoader)
        logger.warning(">>> Loaded config file")
    return config_dict
