# Assessment Backend

Welcome to the backend assessment!

You will be creating a miniature api engine that can talk to a REST backend and provide a graphql frontend to work with. At KittyCo we want everyone to have access to the latest and greatest in kitty information technology.

## Definition of Done

- This assessment will require 3 hours of your time at most.
- Create working, maintainable code that you would feel comfortable with running in production.
- Using python packages is fine and we encourage it, we do not expect you to reinvent the wheel. Just realign it.
- The goal is not to complete all requirements but to see how far you can come while crafting code that meets your own quality criteria.
- We will discuss your creation 3 hours after the start of the assessment.

## Getting started

- We'll assume you have Python 3 installed on your machine.
- Install docker desktop.
- Pull a copy of the repo as your basis.
- Create a venv for this project, be sure to keep your requirements.txt updated.

## Requirements

1. Build a rest api consumer for <https://cat-fact.herokuapp.com/facts>.
2. Create a graphql schema that represents the cat facts obtained by the above api.
3. Build a graphql server that listens to a getCatFact query and returns a Cat Fact.
4. Deploy a graphql playground.
5. Build a docker container that can run the above.
6. Show a working graphql playground that can successfully run a getCatFact call.

## Delivery

- Place your code in a public github/gitlab/etc repo and share the link to the person taking the assesment. If you're having trouble with that, email is ok too.

> Good luck!

(version 2022-02)
