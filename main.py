import logging
import sys
import helpers as helpers
from logging import StreamHandler, Formatter


def main():
    # Read the configuration file in to a dict
    config_dict = helpers.read_configuration_file()

    # Setup the main thread logger with the parameters from the config file
    logger = logging.getLogger()
    logger.setLevel(config_dict["log_level"])
    handler = StreamHandler(stream=sys.stdout)
    handler.setFormatter(Formatter(fmt=config_dict["log_format"]))
    logger.addHandler(handler)
    logger.warning("Starting app")

    source_url = config_dict["source_url"]


if __name__ == '__main__':
    main()
